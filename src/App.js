import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Hello! it is Olgas first React App.
        </p>
        <a
          className="App-link"
          href="https://ai.vizago.lv/portal/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Go to Course Space
        </a>
      </header>
    </div>
  );
}

export default App;
